# AdvancedScene

#### Description
一个基于C++17的简易脚本语言。
最早是给我的游戏引擎设计的，嗯，我也用过lua，结果发现lua不合我的心意。于是自己做了一个。
性能还不错。

#### 源代码结构
语言的核心一共有三个主要的头文件，这三个头文件的有不同的用途

1. AdvancedScene.h 这个头文件用于实现AScene脚本语言的功能。
2. JE_Basic.h 这头文件是从我的游戏引擎里面抠下来的，AScene主要使用了里面的一些容器和函数，例如线程安全的map等。
3. ascene.h 用于其他项目，如果你想在你的其他项目中使用AScene，你就可能需要这个头文件，并结合dll和lib使用（linux就是.so啦）。

其他文件用于实现其他零散的功能。
其中cJSON来自Dave Gamble，cJsonObject来自bwarliao ，用于实现json序列化和反序列化。
AdvancedScene.cpp用于实现部分功能，包括语言的初始化。


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
